FROM node:latest as builder

RUN mkdir -p /app
WORKDIR /app

COPY . .

FROM nginx:alpine
COPY src/nginx/etc/conf.d/default.conf /etc/nginx/conf/default.conf
COPY --from=builder app/dist/angular8-crud-demo usr/share/nginx/html

#RUN node_modules/.bin/ng build --prod
