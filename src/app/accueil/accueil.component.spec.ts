import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AccueilComponent } from './accueil.component';

describe('AccueilComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AccueilComponent
      ],
    }).compileComponents();
  }));

  it('should create the accueil', () => {
    const fixture = TestBed.createComponent(AccueilComponent);
    const accueil = fixture.componentInstance;
    expect(accueil).toBeTruthy();
  });

  it(`should have as title 'angular8-crud-demo'`, () => {
    const fixture = TestBed.createComponent(AccueilComponent);
    const accueil = fixture.componentInstance;
    expect(accueil.title).toEqual('angular8-crud-demo');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AccueilComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('angular8-crud-demo accueil is running!');
  });
});
