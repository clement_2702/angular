import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateUsagerComponent } from './usager/create-usager/create-usager.component';
import { UsagerListComponent } from './usager/usager-list/usager-list.component';
import { UpdateUsagerComponent } from './usager/update-usager/update-usager.component';

import {OeuvreListComponent } from './oeuvre/oeuvre-list/oeuvre-list.component';
import {CreateLivreComponent} from "./oeuvre/create-livre/create-livre.component";
import {CreateMagazineComponent} from "./oeuvre/create-magazine/create-magazine.component";
import {UpdateLivreComponent} from "./oeuvre/update-livre/update-livre.component";
import {UpdateMagazineComponent} from "./oeuvre/update-magazine/update-magazine.component";

import { ExemplaireListComponent } from './exemplaire/exemplaire-list/exemplaire-list.component';
import { UpdateExemplaireComponent } from './exemplaire/update-exemplaire/update-exemplaire.component';

import {AppFrontComponent} from "./front/appFront.component";
import {AppBackComponent} from "./back/appBack.component";
import {AccueilComponent} from "./accueil/accueil.component";

import {CreateEmpruntComponent} from "./emprunt/create-emprunt/create-emprunt.component";
import {EmpruntListComponent} from "./emprunt/emprunt-list/emprunt-list.component";
import {CreateEmprunt2Component} from "./emprunt/create-emprunt-2/create-emprunt-2.component";
import {CreateEmprunt3Component} from "./emprunt/create-emprunt-3/create-emprunt-3.component";
import {RendreEmpruntComponent} from "./emprunt/rendre-emprunt/rendre-emprunt.component";

import {ReservationListComponent} from "./reservation/reservation-list/reservation-list.component";
import {CreateReservationComponent} from "./reservation/create-reservation/create-reservation.component";
import {CreateReservationComponent2} from "./reservation/create-reservation-2/create-reservation-2.component";


const routes: Routes = [
  { path: '', redirectTo: 'usager', pathMatch: 'full' },

  { path: 'add', component: CreateUsagerComponent },
  { path: 'usagers', component: UsagerListComponent },
  { path: 'update/:id', component: UpdateUsagerComponent },

  { path: 'oeuvres', component: OeuvreListComponent },
  { path: 'addLivre', component: CreateLivreComponent },
  { path: 'addMagazine', component: CreateMagazineComponent },
  { path: 'updateLivre/:id', component: UpdateLivreComponent },
  { path: 'updateMagazine/:id', component: UpdateMagazineComponent },

  { path: 'exemplaires/:id', component: ExemplaireListComponent },
  { path: 'updateExemplaire/:id/:idExemplaire', component: UpdateExemplaireComponent },

  { path: 'addEmprunt', component: CreateEmpruntComponent },
  { path: 'emprunts', component: EmpruntListComponent },
  { path: 'emprunts/:usagerNom/:oeuvreTitre', component: EmpruntListComponent },
  { path: 'addEmprunt2/:usagerId', component: CreateEmprunt2Component },
  { path: 'addEmprunt3/:usagerId/:oeuvreId', component: CreateEmprunt3Component },
  { path: 'rendreEmprunt/:empruntId', component: RendreEmpruntComponent },

  { path: 'reservations', component: ReservationListComponent },
  { path: 'addReservation', component: CreateReservationComponent },
  { path: 'addReservation2/:usagerId', component: CreateReservationComponent2 },

  { path: 'back', component: AppBackComponent },
  { path: 'front', component: AppFrontComponent },
  { path: 'accueil', component: AccueilComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
