import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { UsagerService } from './service/usager.service';
import { CreateUsagerComponent } from './usager/create-usager/create-usager.component';
import { UsagerListComponent } from './usager/usager-list/usager-list.component';
import { UpdateUsagerComponent } from './usager/update-usager/update-usager.component';

import { OeuvreService } from './service/oeuvre.service';
import { OeuvreListComponent } from './oeuvre/oeuvre-list/oeuvre-list.component';

import { ExemplaireService } from './service/exemplaire.service';
import { ExemplaireListComponent } from './exemplaire/exemplaire-list/exemplaire-list.component';
import { UpdateExemplaireComponent } from './exemplaire/update-exemplaire/update-exemplaire.component';

import { DataTablesModule } from 'angular-datatables';

import {AccueilComponent} from "./accueil/accueil.component";
import {AppBackComponent} from "./back/appBack.component";
import {AppFrontComponent} from "./front/appFront.component";

import {EmpruntService} from "./service/emprunt.service";
import {CreateEmpruntComponent} from "./emprunt/create-emprunt/create-emprunt.component";
import {EmpruntListComponent} from "./emprunt/emprunt-list/emprunt-list.component";
import {CreateEmprunt3Component} from "./emprunt/create-emprunt-3/create-emprunt-3.component";
import {CreateEmprunt2Component} from "./emprunt/create-emprunt-2/create-emprunt-2.component";
import {EmpruntListUsagerPipe} from "./emprunt/emprunt-list/emprunt-list-usager.pipe";
import {EmpruntListOeuvrePipe} from "./emprunt/emprunt-list/emprunt-list-oeuvre.pipe";

import {ReservationListComponent} from "./reservation/reservation-list/reservation-list.component";
import {CreateReservationComponent} from "./reservation/create-reservation/create-reservation.component";
import {CreateReservationComponent2} from "./reservation/create-reservation-2/create-reservation-2.component";
import {ReservationService} from "./service/reservation.service";
import {ReservationListUsagerPipe} from "./reservation/reservation-list/reservation-list-usager.pipe";
import {ReservationListOeuvrePipe} from "./reservation/reservation-list/reservation-list-oeuvre.pipe";
import {RendreEmpruntComponent} from "./emprunt/rendre-emprunt/rendre-emprunt.component";

import {CreateLivreComponent} from "./oeuvre/create-livre/create-livre.component";
import {LivreService} from "./service/livre.service";
import {CreateMagazineComponent} from "./oeuvre/create-magazine/create-magazine.component";
import {MagazineService} from "./service/magazine.service";
import {UpdateLivreComponent} from "./oeuvre/update-livre/update-livre.component";
import {UpdateMagazineComponent} from "./oeuvre/update-magazine/update-magazine.component";

import { AlertModule } from './alert/alert.module';



@NgModule({
  declarations: [
    AppComponent,
    AppBackComponent,
    AppFrontComponent,
    AccueilComponent,

    CreateUsagerComponent,
    UsagerListComponent,
    UpdateUsagerComponent,

    CreateLivreComponent,
    CreateMagazineComponent,
    OeuvreListComponent,
    UpdateLivreComponent,
    UpdateMagazineComponent,

    ExemplaireListComponent,
    UpdateExemplaireComponent,

    CreateEmpruntComponent,
    EmpruntListComponent,
    CreateEmprunt2Component,
    CreateEmprunt3Component,
    EmpruntListUsagerPipe,
    EmpruntListOeuvrePipe,

    ReservationListComponent,
    CreateReservationComponent,
    CreateReservationComponent2,
    RendreEmpruntComponent,
    ReservationListUsagerPipe,
    ReservationListOeuvrePipe

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    DataTablesModule,
    AlertModule
  ],
  providers: [
    UsagerService,
    LivreService,
    MagazineService,
    OeuvreService,
    ExemplaireService,
    EmpruntService,
    ReservationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
