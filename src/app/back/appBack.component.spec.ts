import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppBackComponent } from './appBack.component';

describe('AppBackComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppBackComponent
      ],
    }).compileComponents();
  }));

  it('should create the appBack', () => {
    const fixture = TestBed.createComponent(AppBackComponent);
    const appBack = fixture.componentInstance;
    expect(appBack).toBeTruthy();
  });

  it(`should have as title 'angular8-crud-demo'`, () => {
    const fixture = TestBed.createComponent(AppBackComponent);
    const appBack = fixture.componentInstance;
    expect(appBack.title).toEqual('angular8-crud-demo');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppBackComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('angular8-crud-demo appBack is running!');
  });
});
