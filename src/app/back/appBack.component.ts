import { Component } from '@angular/core';

@Component({
  selector: 'app-back',
  templateUrl: './appBack.component.html',
  styleUrls: ['./appBack.component.css']
})
export class AppBackComponent {
  title = 'angular8-crud-demo';
}
