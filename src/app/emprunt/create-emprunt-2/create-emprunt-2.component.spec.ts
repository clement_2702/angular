import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmprunt2Component } from './create-emprunt-2.component';

describe('CreateEmprunt2Component', () => {
  let component: CreateEmprunt2Component;
  let fixture: ComponentFixture<CreateEmprunt2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmprunt2Component ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmprunt2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
