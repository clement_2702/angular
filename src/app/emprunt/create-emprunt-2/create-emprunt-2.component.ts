
import { OeuvreService } from 'src/app/service/oeuvre.service';
import { Emprunt } from 'src/app/model/emprunt.model';
import { Oeuvre } from 'src/app/model/oeuvre.model';

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from "rxjs";
import {ApiResponse} from "../../model/api.response";


@Component({
  selector: 'app-create-emprunt-2',
  templateUrl: './create-emprunt-2.component.html',
  styleUrls: ['./create-emprunt-2.component.css']
})
export class CreateEmprunt2Component implements OnInit {

  emprunt: Emprunt = new Emprunt();
  usagerId: number;
  oeuvres: Observable<ApiResponse>;

  constructor(private route: ActivatedRoute,
              private oeuvreService: OeuvreService,
              private router: Router) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },50);
  }

  ngOnInit() {
    this.usagerId = this.route.snapshot.params['usagerId'];
    this.oeuvres = this.oeuvreService.getOeuvres();
  }

  selectionExemplaireEmprunt(oeuvreId: number){
    this.router.navigate(['addEmprunt3/' + this.usagerId + '/' + oeuvreId]);
  }
}
