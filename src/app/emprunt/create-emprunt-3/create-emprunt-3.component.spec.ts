import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmprunt3Component } from './create-emprunt-3.component';

describe('CreateEmprunt3Component', () => {
  let component: CreateEmprunt3Component;
  let fixture: ComponentFixture<CreateEmprunt3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmprunt3Component ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmprunt3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
