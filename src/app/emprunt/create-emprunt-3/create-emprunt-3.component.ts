import { Component, OnInit, ViewChild } from '@angular/core';
import { EmpruntService } from 'src/app/service/emprunt.service';
import { ExemplaireService } from 'src/app/service/exemplaire.service';

import { Observable } from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {Oeuvre} from "../../model/oeuvre.model";
import {Emprunt} from "../../model/emprunt.model";
import {Exemplaire} from "../../model/exemplaire.model";
import {ReservationService} from "../../service/reservation.service";

@Component({
  selector: 'app-create-emprunt-3',
  templateUrl: './create-emprunt-3.component.html',
  styleUrls: ['./create-emprunt-3.component.css']
})
export class CreateEmprunt3Component implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  usagerId: number;
  oeuvreId: number;
  emprunt: Emprunt = new Emprunt();
  exemplaires: Observable<ApiResponse>;
  private error: any;
  private exemplaire: Exemplaire;

  constructor(private route: ActivatedRoute,private router: Router,
              private empruntService: EmpruntService,
              private reservationService: ReservationService,
              private exemplaireService: ExemplaireService) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },50);
  }

  ngOnInit() {
    this.usagerId = this.route.snapshot.params['usagerId'];
    this.oeuvreId = this.route.snapshot.params['oeuvreId'];
    this.exemplaires = this.exemplaireService.getExemplaireByOeuvreIdDispo(this.oeuvreId);
  }

  addEmprunt(exemplaireId: number){
    this.emprunt.usager_id=this.usagerId;
    this.emprunt.exemplaire_id=exemplaireId;

    this.empruntService.createEmprunt(this.emprunt)
      .subscribe(data => console.log(data), error => {
        console.log(error);
        this.error = error;
      });

    if (!this.error) {
      this.deleteReservationWhereOeuvreAndUsager();
      this.setExemplaireEtat(exemplaireId);
    }
  }

  deleteReservationWhereOeuvreAndUsager() {
    this.reservationService.deleteWhereOeuvreAndUsager(this.oeuvreId, this.usagerId)
      .subscribe(data => console.log(data), error => {
        console.log(error);
        this.error = error;
      });
  }

  setExemplaireEtat(exemplaireId: number) {
    this.exemplaire = new Exemplaire();
    this.exemplaire.id = exemplaireId;
    this.exemplaire.etat = "Emprunté";
    this.exemplaireService.updateExemplaire(exemplaireId, this.exemplaire)
      .subscribe(data => {
        console.log(data);
        this.emprunt = new Emprunt();
        this.router.navigate(['emprunts']);
      }, error => {
        console.log(error);
        this.error = error;
      });
  }

  addEmprunt2() {
    this.router.navigate(['addEmprunt2/'+this.usagerId]);
  }
}
