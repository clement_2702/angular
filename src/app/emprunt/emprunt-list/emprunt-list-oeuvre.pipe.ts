import { Pipe, PipeTransform } from '@angular/core';
import {Emprunt} from "../../model/emprunt.model";
import {UsagerService} from "../../service/usager.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import { ApiResponse } from '../../model/api.response';
import {OeuvreService} from "../../service/oeuvre.service";
import {Router} from "@angular/router";
import {map} from "rxjs/operators";
import {ExemplaireService} from "../../service/exemplaire.service";

@Pipe ({
  name: 'empruntOeuvre'
})

export class EmpruntListOeuvrePipe implements PipeTransform {

  oeuvreId: Observable<any>;
  private oeuvreService: OeuvreService;

  constructor(private http: HttpClient) {
    this.oeuvreService = new OeuvreService(http);
  }

  transform (emprunt: Emprunt, args ?: any): any {
    this.oeuvreService=new OeuvreService(this.http);
    if (emprunt) {
      return this.oeuvreService.getOeuvreByExemplaireId(emprunt.exemplaire_id).pipe(map(data => {
        return data.titre;
      }));
    }
  }
}
