import { Pipe, PipeTransform } from '@angular/core';
import {Emprunt} from "../../model/emprunt.model";
import {UsagerService} from "../../service/usager.service";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Pipe ({
  name: 'empruntUsager'
})

export class EmpruntListUsagerPipe implements PipeTransform {
  private usagerService: UsagerService;

  constructor(private http: HttpClient) {
    this.usagerService = new UsagerService(http);
  }

  transform (emprunt: Emprunt, args ?: any): any {
    this.usagerService=new UsagerService(this.http);
    if (emprunt) {
      return this.usagerService.getUsagerById(emprunt.usager_id).pipe(map(data => {
          return (data.nom + " " + data.prenom);
      }));
    }
    return "-";
  }
}
