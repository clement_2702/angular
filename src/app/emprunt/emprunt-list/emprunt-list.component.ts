import {Component, OnInit, Pipe, ViewChild} from '@angular/core';
import { EmpruntService } from 'src/app/service/emprunt.service';
import {Observable, of} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {UsagerService} from "../../service/usager.service";
import {OeuvreService} from "../../service/oeuvre.service";
import {Usager} from "../../model/usager.model";
import {Oeuvre} from "../../model/oeuvre.model";

@Component({
  selector: 'app-emprunt-list',
  templateUrl: './emprunt-list.component.html',
  styleUrls: ['./emprunt-list.component.css']
})


export class EmpruntListComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  emprunts: Observable<ApiResponse>;
  usager: Usager;
  usagerNom: String;
  oeuvreTitre: String;
  submitted = false;
  oeuvre: Oeuvre = new Oeuvre();

  constructor(private empruntService: EmpruntService,
              private usagerService: UsagerService,
              private oeuvreService: OeuvreService,
              private route: ActivatedRoute,
              private router: Router) {

    this.emprunts = this.empruntService.getEmprunts();
  }

  ngOnInit() {
    this.usagerNom="";
    this.oeuvreTitre="";
    this.emprunts = new Observable<ApiResponse>();
    this.emprunts = this.empruntService.getEmprunts();
  }

  onSubmit() {
    this.submitted = true;
    this.emprunts = this.empruntService.getEmpruntsByUsagerNom(this.usagerNom);
  }

  getEmpruntByUsagerNameAndOeuvreTitre() {
    if (this.usagerNom!="" && this.oeuvreTitre!="") {
      this.emprunts = this.empruntService.getEmpruntByUsagerNameAndOeuvreTitre(this.usagerNom, this.oeuvreTitre);
    }
    else if (this.usagerNom!="") {
      this.emprunts = this.empruntService.getEmpruntsByUsagerNom(this.usagerNom);
    }
    else if (this.oeuvreTitre!="") {
      this.emprunts = this.empruntService.getEmpruntsByOeuvreTitre(this.oeuvreTitre);
    }
    else {
      this.emprunts = this.empruntService.getEmprunts();
    }
  }

  clear() {
    this.usagerNom="";
    this.emprunts = this.empruntService.getEmprunts();
  }

  updateEmprunt(id: number){
    this.router.navigate(['updateEmprunt', id]);
  }

  rendreOeuvre(id: number){
    this.router.navigate(['rendreEmprunt/'+id]);
  }
}
