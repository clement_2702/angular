import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendreEmpruntComponent } from './rendre-emprunt.component';

describe('RendreEmpruntComponent', () => {
  let component: RendreEmpruntComponent;
  let fixture: ComponentFixture<RendreEmpruntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendreEmpruntComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendreEmpruntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
