import {Component, OnInit, Pipe, ViewChild} from '@angular/core';
import { EmpruntService } from 'src/app/service/emprunt.service';
import {Observable, of} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {UsagerService} from "../../service/usager.service";
import {OeuvreService} from "../../service/oeuvre.service";
import {Usager} from "../../model/usager.model";
import {Oeuvre} from "../../model/oeuvre.model";
import {Emprunt} from "../../model/emprunt.model";
import {Exemplaire} from "../../model/exemplaire.model";
import {ExemplaireService} from "../../service/exemplaire.service";
import {AlertService} from "../../service/alert.service";

@Component({
  selector: 'app-rendre-emprunt',
  templateUrl: './rendre-emprunt.component.html',
  styleUrls: ['./rendre-emprunt.component.css']
})


export class RendreEmpruntComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  emprunt: Emprunt;
  choix: string;
  empruntId: number;
  exemplaire: Exemplaire=new Exemplaire();
  error: any;


  constructor(public alertService: AlertService,
              private empruntService: EmpruntService,
              private exemplaireService: ExemplaireService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    this.empruntId = this.route.snapshot.params['empruntId'];
    this.empruntService.getEmpruntById(this.empruntId)
      .subscribe(
        data => {
          console.log(data);
          this.emprunt=data;
        },
        error => {
          console.log(error);
          this.error = error});
  }

  deleteEmprunt() {
    this.empruntService.deleteEmprunt(this.empruntId)
      .subscribe(
        data => {
          console.log(data);
          this.updateExemplaireEtatDispo(this.emprunt.exemplaire_id);
        },
        error => {
          console.log(error);
          this.error = error});
  }

  isEtat() {
    if (this.choix=='Bon état' || this.choix=='Mauvais état') {
      this.exemplaire.etat=this.choix;
      console.log(this.exemplaire.etat)
      this.deleteEmprunt();
    } else {
      this.alertService.error('Erreur : choisissez l\'etat de l\'oeuvre', this.options);
    }
  }

  updateExemplaireEtatDispo(exemplaire_id: number) {
    this.exemplaire.id=exemplaire_id;
    this.exemplaire.oeuvreId=0;
    console.log(this.exemplaire);
    this.exemplaireService.updateExemplaire(exemplaire_id, this.exemplaire)
      .subscribe(
      data => {
        console.log(data);
        this.alertService.success('Exemplaire rendu', this.options);
        this.exemplaire=new Exemplaire();
        this.router.navigate(['emprunts']);
      },
      error => {
        console.log(error);
      });
  }
}
