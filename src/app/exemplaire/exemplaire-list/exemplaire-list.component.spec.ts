import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExemplaireListComponent } from './exemplaire-list.component';

describe('ExemplaireListComponent', () => {
  let component: ExemplaireListComponent;
  let fixture: ComponentFixture<ExemplaireListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExemplaireListComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExemplaireListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
