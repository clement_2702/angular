import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import { Exemplaire } from 'src/app/model/exemplaire.model';
import { ExemplaireService } from 'src/app/service/exemplaire.service';
import { Observable } from 'rxjs';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router} from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {OeuvreService} from "../../service/oeuvre.service";
import {Oeuvre} from "../../model/oeuvre.model";
import {AlertService} from "../../service/alert.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-exemplaire-list',
  templateUrl: './exemplaire-list.component.html',
  styleUrls: ['./exemplaire-list.component.css']
})
export class ExemplaireListComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  id: number;
  exemplaires: Observable<ApiResponse>;
  exemplaire:Exemplaire=new Exemplaire();
  @ViewChild('dtOptions', {static: true}) table;
  result: boolean;

  constructor(private ngZone: NgZone,
              public alertService: AlertService,
              private route: ActivatedRoute,
              private router: Router,
              private exemplaireService: ExemplaireService) {
    route.params.subscribe(val => {
      this.id = this.route.snapshot.params['id'];
      this.exemplaires = this.exemplaireService.getExemplaireByOeuvreId(this.id);
    });
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },50);
  }

  ngOnInit(){
    this.exemplaire=new Exemplaire();
    this.id = this.route.snapshot.params['id'];
    this.exemplaires = this.exemplaireService.getExemplaireByOeuvreId(this.id);
  }

  deleteExemplaire(id: number) {
    this.exemplaireService.deleteExemplaire(id)
      .subscribe(
        data => {
          console.log(data);
          this.exemplaires = this.exemplaireService.getExemplaireByOeuvreId(this.id);
        },
        error => console.log(error));
    this.alertService.success('Exemplaire supprimé', this.options);
    this.router.navigate(['/exemplaires/', this.id]);
  }

  isEmprunt(id: number){
    return this.exemplaireService.getExemplaireByIdAndEtat(id)
      .subscribe(
        data => {
          if (data) {
            this.alertService.error('Erreur : cet exemplaire est encore empruntée', this.options);
          } else {
            this.deleteExemplaire(id);
          }
        },
        error => console.log(error));
  }

  updateExemplaire(id: number){
    this.router.navigate(['/updateExemplaire/'+ this.id+"/"+id ]);
  }

  addExemplaire(){
    this.id = this.route.snapshot.params['id'];
    this.exemplaire.oeuvreId = this.route.snapshot.params['id'];
    this.exemplaire.etat='Bon état';
    this.exemplaireService.createExemplaire(this.exemplaire)
      .subscribe(data => {
        console.log(data);
        this.exemplaire = new Exemplaire();
        this.alertService.success('Ajout exemplaire reussi', this.options);
        this.exemplaires = this.exemplaireService.getExemplaireByOeuvreId(this.id);
      }, error => console.log(error));
  }
}
