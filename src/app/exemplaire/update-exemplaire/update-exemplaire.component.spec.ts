import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateExemplaireComponent } from './update-exemplaire.component';

describe('UpdateExemplaireComponent', () => {
  let component: UpdateExemplaireComponent;
  let fixture: ComponentFixture<UpdateExemplaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateExemplaireComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateExemplaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
