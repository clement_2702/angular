import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { Exemplaire } from 'src/app/model/exemplaire.model';
import { ExemplaireService } from 'src/app/service/exemplaire.service';
import { ApiResponse } from 'src/app/model/api.response';

@Component({
  selector: 'app-update-exemplaire',
  templateUrl: './update-exemplaire.component.html',
  styleUrls: ['./update-exemplaire.component.css']
})
export class UpdateExemplaireComponent implements OnInit {

  id: number;
  idExemplaire: number;
  exemplaire: Exemplaire;

  constructor(private route: ActivatedRoute,private router: Router,
              private exemplaireService: ExemplaireService) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }

  ngOnInit() {
    this.exemplaire = new Exemplaire();
    this.id = this.route.snapshot.params['id'];
    this.idExemplaire = this.route.snapshot.params['idExemplaire'];
    this.exemplaireService.getExemplaireById(this.idExemplaire)
      .subscribe(data => {
        console.log(data)
        this.exemplaire = data;
      }, error => console.log(error));
  }

  onSubmit() {
    this.exemplaireService.updateExemplaire(this.idExemplaire, this.exemplaire)
      .subscribe(data => {
        console.log(data);
        this.exemplaire = new Exemplaire();
        this.router.navigate(['/exemplaires/',this.id]);
      }, error => console.log(error));

  }

  annuler() {
    this.router.navigate(['/exemplaires/', this.id], { relativeTo: this.route });
  }
}
