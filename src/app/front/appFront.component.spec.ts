import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppFrontComponent } from './appFront.component';

describe('AppFrontComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppFrontComponent
      ],
    }).compileComponents();
  }));

  it('should create the appFront', () => {
    const fixture = TestBed.createComponent(AppFrontComponent);
    const appFront = fixture.componentInstance;
    expect(appFront).toBeTruthy();
  });

  it(`should have as title 'angular8-crud-demo'`, () => {
    const fixture = TestBed.createComponent(AppFrontComponent);
    const appFront = fixture.componentInstance;
    expect(appFront.title).toEqual('angular8-crud-demo');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppFrontComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('angular8-crud-demo appFront is running!');
  });
});
