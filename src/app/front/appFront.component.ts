import { Component } from '@angular/core';

@Component({
  selector: 'appFront',
  templateUrl: './appFront.component.html',
  styleUrls: ['./appFront.component.css']
})
export class AppFrontComponent {
  title = 'Front';
}
