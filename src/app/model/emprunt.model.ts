export class Emprunt {
  id: number;
  exemplaire_id:number;
  usager_id: number;
  actif: number;
  date: Date;
}
