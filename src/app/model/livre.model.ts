export class Livre {
  id: number;
  resume: string;
  date_edition: Date;
  auteur: String;
}
