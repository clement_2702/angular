export class Magazine {
  id: number;
  numero: number
  periodicite: number;
  oeuvre_id: number;
  date_parution: Date;
  auteur: String;
}
