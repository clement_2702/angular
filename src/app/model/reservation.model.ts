export class Reservation {
	id: number;
	date_reservation: Date;
	fk_usager_id: number;
	fk_oeuvre_id: number;
}