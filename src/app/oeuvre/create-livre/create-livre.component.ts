
import { OeuvreService } from 'src/app/service/oeuvre.service';
import { Oeuvre } from 'src/app/model/oeuvre.model';

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {Router} from '@angular/router';
import {Livre} from "../../model/livre.model";
import {LivreService} from "../../service/livre.service";
import {AlertService} from "../../service/alert.service";


@Component({
  selector: 'app-create-oeuvre',
  templateUrl: './create-livre.component.html',
  styleUrls: ['./create-livre.component.css']
})
export class CreateLivreComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  date_edition:String='';
  livre: Livre = new Livre();
  oeuvre: Oeuvre = new Oeuvre();

  constructor(public alertService: AlertService,
              private oeuvreService: OeuvreService,
              private livreService: LivreService,
              private router: Router) {}

  ngOnInit() {

  }

  createLivre() {
    if ((/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/.test(this.date_edition+'')) &&
      this.livre.resume && this.livre.auteur && this.oeuvre.titre) {

      this.livre.date_edition=new Date(this.date_edition+'');
      this.livreService.createLivre(this.livre, this.oeuvre.titre)
        .subscribe(data => {
          console.log(data);
          this.oeuvre = new Oeuvre();
          this.livre = new Livre();
          this.alertService.success('Ajout livre reussi', this.options);
          this.router.navigate(['/oeuvres']);
        }, error => console.log(error));
    } else {
      this.alertService.error('Erreur : un des champs est mal complété ou manquant', this.options);
    }
  }
}












