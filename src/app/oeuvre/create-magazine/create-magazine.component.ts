
import { OeuvreService } from 'src/app/service/oeuvre.service';
import { Oeuvre } from 'src/app/model/oeuvre.model';

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {Router} from '@angular/router';
import {Magazine} from "../../model/magazine.model";
import {MagazineService} from "../../service/magazine.service";
import {AlertService} from "../../service/alert.service";


@Component({
  selector: 'app-create-oeuvre',
  templateUrl: './create-magazine.component.html',
  styleUrls: ['./create-magazine.component.css']
})
export class CreateMagazineComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  date_parution:String;
  magazine: Magazine = new Magazine();
  oeuvre: Oeuvre = new Oeuvre();

  constructor(public alertService: AlertService,
              private oeuvreService: OeuvreService,
              private magazineService: MagazineService,
              private router: Router) {}

  ngOnInit() {

  }

  createMagazine() {
    if ((/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/.test(this.date_parution+'')) &&
      this.magazine.numero && this.magazine.periodicite && this.oeuvre.titre) {

      this.magazine.date_parution=new Date(this.date_parution+'');
      this.magazineService.createMagazine(this.magazine, this.oeuvre.titre)
        .subscribe(data => {
          console.log(data);
          this.oeuvre = new Oeuvre();
          this.magazine = new Magazine();
          this.alertService.success('Ajout magazine reussi', this.options);
          this.router.navigate(['/oeuvres']);
        }, error => console.log(error));
    } else {
      this.alertService.error('Erreur : un des champs est mal complété ou manquant', this.options);
    }
  }

}












