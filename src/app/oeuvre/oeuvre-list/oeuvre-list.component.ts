import { Component, OnInit, ViewChild } from '@angular/core';
import { Oeuvre } from 'src/app/model/oeuvre.model';
import { OeuvreService } from 'src/app/service/oeuvre.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {LivreService} from "../../service/livre.service";
import {AlertService} from "../../service/alert.service";
import {EmpruntService} from "../../service/emprunt.service";
import {Emprunt} from "../../model/emprunt.model";
import {ExemplaireService} from "../../service/exemplaire.service";
import {Exemplaire} from "../../model/exemplaire.model";

@Component({
  selector: 'app-oeuvre-list',
  templateUrl: './oeuvre-list.component.html',
  styleUrls: ['./oeuvre-list.component.css']
})
export class OeuvreListComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  exemplaire: Exemplaire=new Exemplaire();
  emprunt: Emprunt=new Emprunt();
  oeuvres: Observable<ApiResponse>;

  constructor(public alertService: AlertService,
              private oeuvreService: OeuvreService,
              private empruntService: EmpruntService,
              private livreService: LivreService,
              private exemplaireService: ExemplaireService,
              private router: Router) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },200);
  }

  ngOnInit() {
    this.oeuvres = this.oeuvreService.getOeuvres();
  }

  deleteOeuvre(id: number) {
    this.oeuvreService.deleteOeuvre(id)
      .subscribe(
        data => {
          this.oeuvres = this.oeuvreService.getOeuvres();
          this.alertService.success('Suppression reussi', this.options);
        },
        error => console.log(error));

  }

  isExemplaire(id: number){
    this.exemplaireService.getOneExemplaireByOeuvreId(id)
      .subscribe(
        data => {
          if (data) {
            this.alertService.error('Erreur : cette oeuvre possède encore des exemplaires', this.options);
          } else {
            this.deleteOeuvre(id);
          }
        },
        error => console.log(error));
  }

  updateOeuvre(id: number){
    this.livreService.getLivreByOeuvreId(id)
      .subscribe(
        data => {
          if (data) {
            this.router.navigate(['updateLivre', id]);
          } else {
            this.router.navigate(['updateMagazine', id]);
          }
        },
        error => console.log(error));
  }

  voirExemplaire(id: number){
    this.router.navigate(['exemplaires', id]);
  }


}
