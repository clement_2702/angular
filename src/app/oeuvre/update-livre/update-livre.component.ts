import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Livre } from 'src/app/model/livre.model';
import { LivreService } from 'src/app/service/livre.service';
import { ApiResponse } from 'src/app/model/api.response';
import {OeuvreService} from "../../service/oeuvre.service";
import {Oeuvre} from "../../model/oeuvre.model";
import {AlertService} from "../../service/alert.service";

@Component({
  selector: 'app-update-livre',
  templateUrl: './update-livre.component.html',
  styleUrls: ['./update-livre.component.css']
})
export class UpdateLivreComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  id: number;
  date_edition: string;
  livre: Livre;
  oeuvre: Oeuvre;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private livreService: LivreService,
              private alertService: AlertService,
              private oeuvreService: OeuvreService) { }

  ngOnInit() {
    this.livre = new Livre();
    this.oeuvre=new Oeuvre();

    this.id = this.route.snapshot.params['id'];

    this.oeuvreService.getOeuvreById(this.id)
      .subscribe(data => {
        console.log(data);
        this.oeuvre = data;
      }, error => console.log(error));

    this.livreService.getLivreByOeuvreId(this.id)
      .subscribe(data => {
        console.log(data);
        this.livre = data;
      }, error => console.log(error));
  }

  updateLivre() {
    if ((/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/.test(this.livre.date_edition+'')) &&
      this.livre.resume && this.livre.auteur && this.oeuvre.titre) {

      this.livre.date_edition = new Date(this.livre.date_edition + '');
      this.livreService.updateLivre(this.oeuvre.titre, this.livre)
        .subscribe(data => {
          console.log(data);
          this.livre = new Livre();
          this.alertService.success('Le livre a été modifié avec succès reussi', this.options);
          this.router.navigate(['/oeuvres']);
        }, error => console.log(error));
    } else {
      this.alertService.error('Erreur : un des champs est mal complété ou manquant', this.options);
    }
  }

  retour() {
    this.router.navigate(['/oeuvres']);
  }

}
