import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Magazine } from 'src/app/model/magazine.model';
import { MagazineService } from 'src/app/service/magazine.service';
import { ApiResponse } from 'src/app/model/api.response';
import {Oeuvre} from "../../model/oeuvre.model";
import {OeuvreService} from "../../service/oeuvre.service";
import {AlertService} from "../../service/alert.service";

@Component({
  selector: 'app-update-magazine',
  templateUrl: './update-magazine.component.html',
  styleUrls: ['./update-magazine.component.css']
})
export class UpdateMagazineComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  id: number;
  magazine: Magazine;
  date_parution: string;
  oeuvre: Oeuvre;
  apiResponse:ApiResponse;

  constructor(private route: ActivatedRoute,private router: Router,
              private magazineService: MagazineService,
              private alertService: AlertService,
              private oeuvreService: OeuvreService) { }

  ngOnInit() {
    this.magazine = new Magazine();
    this.oeuvre = new Oeuvre();

    this.id = this.route.snapshot.params['id'];

    this.oeuvreService.getOeuvreById(this.id)
      .subscribe(data => {
        console.log(data);
        this.oeuvre = data;
      }, error => console.log(error));

    this.magazineService.getMagazineByOeuvreId(this.id)
      .subscribe(data => {
        console.log(data);
        this.magazine = data;
      }, error => console.log(error));
  }

  updateMagazine() {
    if ((/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/.test(this.magazine.date_parution+'')) &&
      this.magazine.numero && this.magazine.periodicite && this.oeuvre.titre) {

      this.magazine.date_parution=new Date(this.date_parution+'');
      this.magazineService.updateMagazine(this.oeuvre.titre, this.magazine)
        .subscribe(data => {
          console.log(data);
          this.oeuvre = new Oeuvre();
          this.magazine = new Magazine();
          this.alertService.success('Ajout magazine reussi', this.options);
          this.router.navigate(['/oeuvres']);
        }, error => console.log(error));
    } else {
      this.alertService.error('Erreur : un des champs est mal complété ou manquant', this.options);
    }
  }
}
