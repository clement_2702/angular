import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReservationComponent2 } from './create-reservation-2.component';

describe('CreateReservationComponent2', () => {
  let component: CreateReservationComponent2;
  let fixture: ComponentFixture<CreateReservationComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateReservationComponent2 ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReservationComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
