
import { OeuvreService } from '../../service/oeuvre.service';
import { Oeuvre } from '../../model/oeuvre.model';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from "rxjs";
import {ApiResponse} from "../../model/api.response";
import {Reservation} from "../../model/reservation.model";
import {ReservationService} from "../../service/reservation.service";


@Component({
  selector: 'app-create-reservation-2',
  templateUrl: './create-reservation-2.component.html',
  styleUrls: ['./create-reservation-2.component.css']
})
export class CreateReservationComponent2 implements OnInit {

  reservation: Reservation = new Reservation();
  usagerId: number;
  oeuvres: Observable<ApiResponse>;
  private error: any;
  private oeuvre: any;

  constructor(private route: ActivatedRoute,
              private oeuvreService: OeuvreService,
              private reservationService: ReservationService,
              private router: Router) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },50);
  }

  ngOnInit() {
    this.usagerId = this.route.snapshot.params['usagerId'];
    this.oeuvres = this.oeuvreService.getOeuvres();
  }

  addReservation(oeuvreId: number){
    this.reservation.fk_usager_id=this.usagerId;
    this.reservation.fk_oeuvre_id=oeuvreId;

    this.reservationService.createReservation(this.reservation)
      .subscribe(data => {
        console.log(data)
        this.reservation = new Reservation();
        this.router.navigate(['reservations']);
      }, error => {
        console.log(error);
      });
  }
}
