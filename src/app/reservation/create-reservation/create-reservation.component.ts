
import { UsagerService } from 'src/app/service/usager.service';
import { OeuvreService } from 'src/app/service/oeuvre.service';

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from "rxjs";
import {ApiResponse} from "../../model/api.response";


@Component({
  selector: 'app-create-reservation',
  templateUrl: './create-reservation.component.html',
  styleUrls: ['./create-reservation.component.css']
})
export class CreateReservationComponent implements OnInit {

  usagers: Observable<ApiResponse>;
  submitted = false;

  constructor(private route: ActivatedRoute,
              private usagerService: UsagerService,
              private router: Router) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },50);
  }

  ngOnInit() {
    this.usagers = this.usagerService.getUsagers();
  }

  selectionOeuvreReservation(usagerId: number){
    this.router.navigate(['addReservation2/'+ usagerId]);
  }
}












