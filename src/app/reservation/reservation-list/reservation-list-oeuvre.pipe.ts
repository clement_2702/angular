import { Pipe, PipeTransform } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Reservation} from "../../model/reservation.model";
import {OeuvreService} from "../../service/oeuvre.service";

@Pipe ({
  name: 'reservationOeuvre'
})

export class ReservationListOeuvrePipe implements PipeTransform {
  private oeuvreService: OeuvreService;

  constructor(private http: HttpClient) {
    this.oeuvreService = new OeuvreService(http);
  }

  transform (reservation: Reservation, args ?: any): any {
    this.oeuvreService=new OeuvreService(this.http);
    if (reservation) {
      return this.oeuvreService.getOeuvreById(reservation.fk_oeuvre_id).pipe(map(data => {
        return (data.titre);
      }));
    }
  }
}
