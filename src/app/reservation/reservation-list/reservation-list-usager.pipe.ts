import { Pipe, PipeTransform } from '@angular/core';
import {Emprunt} from "../../model/emprunt.model";
import {UsagerService} from "../../service/usager.service";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Reservation} from "../../model/reservation.model";

@Pipe ({
  name: 'reservationUsager'
})

export class ReservationListUsagerPipe implements PipeTransform {
  private usagerService: UsagerService;

  constructor(private http: HttpClient) {
    this.usagerService = new UsagerService(http);
  }

  transform (reservation: Reservation, args ?: any): any {
    this.usagerService=new UsagerService(this.http);
    if (reservation) {
      return this.usagerService.getUsagerById(reservation.fk_usager_id).pipe(map(data => {
        return (data.nom + " " + data.prenom);
      }));
    }
    return "-";
  }
}
