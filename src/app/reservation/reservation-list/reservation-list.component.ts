import { Component, OnInit, ViewChild } from '@angular/core';
import { Reservation } from 'src/app/model/reservation.model';
import { ReservationService } from 'src/app/service/reservation.service';
import {Observable, of} from 'rxjs';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {UsagerService} from "../../service/usager.service";
import {OeuvreService} from "../../service/oeuvre.service";
import {Usager} from "../../model/usager.model";
import {async} from "@angular/core/testing";
import {Oeuvre} from "../../model/oeuvre.model";
import {Exemplaire} from "../../model/exemplaire.model";
import {ExemplaireService} from "../../service/exemplaire.service";
import {error} from "@angular/compiler/src/util";

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {

  reservations: Observable<ApiResponse>;

  usagerRetour: Usager = new Usager();
  oeuvre: Oeuvre = new Oeuvre();
  exemplaire: Exemplaire = new Exemplaire();
  private nom: string;
  private error: any;

  constructor(private reservationService: ReservationService,
              private usagerService: UsagerService,
              private oeuvreService: OeuvreService,
              private exemplaireService: ExemplaireService,
              private router: Router) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },2000);

  }

  ngOnInit() {
    this.reservations = this.reservationService.getReservations();
  }

  deleteReservation(id: number) {
    this.reservationService.deleteReservation(id)
      .subscribe(
        data => {
          console.log(data);
          this.reservations = this.reservationService.getReservations();
        },
        error => {
          console.log(error);
          this.error = error;
        });
    if (!this.error) {
      this.subNbReservation(id);
    }
  }

  subNbReservation(oeuvreId: number) {
    this.oeuvreService.updateOeuvreSubReservation(oeuvreId)
      .subscribe(data => console.log(data), error => {
        console.log(error);
        this.error = error;
      });
  }

  getUsager(idUsager: number){
    let usager: Usager;// = new Usager();
    this.usagerService.getUsagerById(idUsager)
      .subscribe(data => {
          console.log(data);
          usager=data;
        },
        error => console.log(error));
    return 5;
  }

}
