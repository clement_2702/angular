import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Emprunt } from '../model/emprunt.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class EmpruntService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/emprunts';

  getEmprunts() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getEmpruntById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "/" + id);
  }

  createEmprunt(emprunt: Emprunt): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "/", emprunt);
  }

  updateEmprunt(id: number, emprunt: Emprunt): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/emprunts/", emprunt);
  }

  getEmpruntsByOeuvreTitre(oeuvreTitre: String) {
    return this.http.get<ApiResponse>(this.baseUrl + "Oeuvre/" + oeuvreTitre);
  }

  getEmpruntByUsagerNameAndOeuvreTitre(usagerNom: String, oeuvreTitre: String) {
    return this.http.get<ApiResponse>(this.baseUrl + "UsagerOeuvre/" + usagerNom + "/" + oeuvreTitre);
  }

  getEmpruntsByUsagerNom(usagerNom: String) {
    return this.http.get<ApiResponse>(this.baseUrl + "Usager/" + usagerNom);
  }

  getEmpruntByUsagerId(id: number) : Observable<any> {
    return this.http.get<ApiResponse>(this.baseUrl + "UsagerId/" + id);
  }

  getEmpruntByOeuvreId(id: number) : Observable<any> {
    return this.http.get<ApiResponse>(this.baseUrl + "OeuvreId/" + id);
  }

  deleteEmprunt(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "/" + id);
  }
}
