import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Exemplaire } from '../model/exemplaire.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class ExemplaireService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/exemplaires/';



  getExemplaires() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getExemplaireById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + id);
  }

  getExemplaireByOeuvreId(id: number): Observable<any> {
    return this.http.get("http://localhost:8080/api/exemplairesOeuvre/" + id);
  }

  getOneExemplaireByOeuvreId(id: number): Observable<any> {
    return this.http.get("http://localhost:8080/api/exemplairesOeuvreId/" + id);
  }

  createExemplaire(exemplaire: Exemplaire): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl, exemplaire);
  }

  updateExemplaire(id: number, exemplaire: Exemplaire): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/exemplaires/" + id, exemplaire);
  }

  deleteExemplaire(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + id);
  }

  getExemplaireByOeuvreIdDispo(oeuvreId: number) : Observable<any> {
    return this.http.get("http://localhost:8080/api/exemplairesOeuvreDispo/" + oeuvreId);
  }

  getExemplaireByIdAndEtat(id: number) : Observable<any> {
    return this.http.get<ApiResponse>("http://localhost:8080/api/exemplairesEmprunt/" + id);
  }
}
