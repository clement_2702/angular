import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Livre } from '../model/livre.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class LivreService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/livres';



  getLivres() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getLivreById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "/" + id);
  }

  getLivreByExemplaireId(exemplaireId: number): Observable<any> {
    return this.http.get(this.baseUrl + "ExemplaireId/"+ exemplaireId);
  }

  getLivreByOeuvreId(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "OeuvreId/"+ id);

  }

  createLivre(livre: Livre, titre : String): Observable<ApiResponse> {
    return this.http.post<ApiResponse>("http://localhost:8080/api/livresOeuvre/"+titre, livre);
  }

  updateLivre(titre: string, livre: Livre): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/livresOeuvre/" + titre, livre);
  }

  updateLivreAddReservation(id: number, livre: Livre = new Livre()): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/livres/addReservation/"+id, livre);
  }

  updateLivreSubReservation(id: number, livre: Livre = new Livre()): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/livres/subReservation/"+id, livre);
  }

  deleteLivre(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "/" + id);
  }
}
