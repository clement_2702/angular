import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Magazine } from '../model/magazine.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class MagazineService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/magazines';

  getMagazines() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getMagazineById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "/" + id);
  }

  getMagazineByOeuvreId(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "OeuvreId/" + id);
  }

  getMagazineByExemplaireId(exemplaireId: number): Observable<any> {
    return this.http.get(this.baseUrl + "ExemplaireId/"+ exemplaireId);
  }

  createMagazine(magazine: Magazine, titre : String): Observable<ApiResponse> {
    return this.http.post<ApiResponse>("http://localhost:8080/api/magazinesOeuvre/"+titre, magazine);
  }

  updateMagazine(titre : String, magazine: Magazine): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/magazinesOeuvre/" + titre, magazine);
  }

  updateMagazineAddReservation(id: number, magazine: Magazine = new Magazine()): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/magazines/addReservation/"+id, magazine);
  }

  updateMagazineSubReservation(id: number, magazine: Magazine = new Magazine()): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/magazines/subReservation/"+id, magazine);
  }

  deleteMagazine(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "/" + id);
  }
}
