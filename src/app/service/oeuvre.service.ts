import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Oeuvre } from '../model/oeuvre.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class OeuvreService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/oeuvres';



  getOeuvres() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getOeuvreById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "/" + id);
  }

  getOeuvreByExemplaireId(exemplaireId: number): Observable<any> {
    return this.http.get(this.baseUrl + "ExemplaireId/"+ exemplaireId);
  }

  createOeuvre(oeuvre: Oeuvre): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl, oeuvre);
  }

  updateOeuvre(id: number, oeuvre: Oeuvre): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/oeuvres/" + id, oeuvre);
  }

  updateOeuvreAddReservation(id: number, oeuvre: Oeuvre = new Oeuvre()): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/oeuvres/addReservation/"+id, oeuvre);
  }

  updateOeuvreSubReservation(id: number, oeuvre: Oeuvre = new Oeuvre()): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/oeuvres/subReservation/"+id, oeuvre);
  }

  deleteOeuvre(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "MagLivre/" + id);
  }
}
