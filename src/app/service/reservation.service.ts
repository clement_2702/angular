import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Reservation } from '../model/reservation.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class ReservationService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/reservations';

  getReservations() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getReservationById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "/" + id);
  }

  createReservation(reservation: Reservation): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "/", reservation);
  }

  updateReservation(id: number, reservation: Reservation): Observable<ApiResponse> {
    return this.http.put<ApiResponse>( "http://localhost:8080/api/reservations/", reservation);
  }

  deleteReservation(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "/" + id);
  }

  deleteWhereOeuvreAndUsager(oeuvreId: number, usagerId: number) {
    return this.http.delete<ApiResponse>(this.baseUrl + "OeuvreAndUsager/" + oeuvreId + "/" + usagerId);
  }
}
