import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Usager } from '../model/usager.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class UsagerService {

  constructor(private http: HttpClient) { }
  private baseUrl: string = 'http://localhost:8080/api/usagers';



  getUsagers() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl);
  }

  getUsagerById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + "/" + id);
  }

  createUsager(usager: Usager): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl, usager);
  }

  updateUsager(id: number, usager: Usager): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + "/" + usager.id, usager);
  }

  deleteUsager(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "/" + id);
  }
}
