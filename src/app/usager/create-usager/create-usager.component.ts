
import { UsagerService } from 'src/app/service/usager.service';
import { Usager } from 'src/app/model/usager.model';

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {Router} from '@angular/router';
import {AlertService} from "../../service/alert.service";


@Component({
  selector: 'app-create-usager',
  templateUrl: './create-usager.component.html',
  styleUrls: ['./create-usager.component.css']
})
export class CreateUsagerComponent implements OnInit {

  usager: Usager = new Usager();
  submitted = false;
  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(private usagerService: UsagerService,
              private router: Router,
              private alertService: AlertService) { }

  ngOnInit() {
  }


  onSubmit() {
    this.submitted = true;
    this.usagerService.createUsager(this.usager)
      .subscribe(data => {
        console.log(data);
        this.alertService.success('Création reussi', this.options);
        this.usager = new Usager();
        this.router.navigate(['/usagers']);
      }, error => console.log(error));
  }
}












