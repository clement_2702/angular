import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usager } from 'src/app/model/usager.model';
import { UsagerService } from 'src/app/service/usager.service';
import { ApiResponse } from 'src/app/model/api.response';
import {AlertService} from "../../service/alert.service";

@Component({
  selector: 'app-update-usager',
  templateUrl: './update-usager.component.html',
  styleUrls: ['./update-usager.component.css']
})
export class UpdateUsagerComponent implements OnInit {

  id: number;
  usager: Usager;
  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(private route: ActivatedRoute,private router: Router,
              private usagerService: UsagerService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.usager = new Usager();

    this.id = this.route.snapshot.params['id'];
    this.usagerService.getUsagerById(this.id)
      .subscribe(data => {
        console.log(data)
        this.usager = data;
      }, error => console.log(error));
  }

  onSubmit() {
    this.usagerService.updateUsager(this.id, this.usager)
      .subscribe(data => {
        console.log(data);
        this.usager = new Usager();
        this.alertService.success('Modification reussi', this.options);
        this.router.navigate(['/usagers']);
      }, error => console.log(error));
  }
}
