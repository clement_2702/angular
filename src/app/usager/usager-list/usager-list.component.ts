import { Component, OnInit, ViewChild } from '@angular/core';
import { Usager } from 'src/app/model/usager.model';
import { UsagerService } from 'src/app/service/usager.service';
import {Observable, Subscription} from 'rxjs';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api.response';
import {EmpruntService} from "../../service/emprunt.service";
import {AlertService} from "../../service/alert.service";
import {Emprunt} from "../../model/emprunt.model";

@Component({
  selector: 'app-usager-list',
  templateUrl: './usager-list.component.html',
  styleUrls: ['./usager-list.component.css']
})
export class UsagerListComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  usagers: Observable<ApiResponse>;
  emprunt: Emprunt=new Emprunt();


  constructor(private usagerService: UsagerService,
              private empruntService: EmpruntService,
              public alertService: AlertService,
              private router: Router) {
    setTimeout(function(){
      $(function(){
        $('#example').DataTable();
      });
    },50);

  }

  ngOnInit() {
    this.usagers = this.usagerService.getUsagers();
  }

  deleteUsager(id: number) {
    this.usagerService.deleteUsager(id)
      .subscribe(
        data => {
          this.usagers = this.usagerService.getUsagers();
          this.alertService.success('Suppression reussi', this.options);
        },
        error => console.log(error));
  }

  updateUsager(id: number){
    this.router.navigate(['update', id]);
  }


  isEmprunt(id: number) {
    this.empruntService.getEmpruntByUsagerId(id)
      .subscribe(
        data => {
          if (data) {
            this.alertService.error('Erreur : cet usager a encore un emprunt', this.options);
          } else {
            this.deleteUsager(id);
          }
          },
        error => console.log(error));
  }
}
